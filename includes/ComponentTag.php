<?php

namespace MediaWiki\Extension\ComponentTemplate;

use Parser;
use PPFrame;
use UtfNormal\Validator;

class ComponentTag {

	public static function parse(
		?string $content, array $attributes, Parser $parser, PPFrame $frame
	) : string {
		$isTransclusion = ( $frame->depth > 0 );

		$component = Component::newFromContent( $content );

		if ( $isTransclusion ) {
			foreach ( $component->getScripts() as $script ) {
				if ( $script->attributes->getNamedItem( 'lang' )->nodeValue === 'lua' ) {
					// TODO:
					// * guard on Scribunto's existence.
					// * less intrusive override to find module code inside a tag
					global $wgScribuntoEngineConf;
					$engine = new ComponentAwareScribunto( [
						// 'errorFile' => '/tmp/lua.err',
						'parser' => $parser,
						'title' => $parser->getTitle(),
					] + $wgScribuntoEngineConf['luastandalone'] );

					$componentTitle = $frame->getTitle();
					$module = $engine->directlyInjectModule( $script->nodeValue, $componentTitle );
					// TODO: store component title to global or as arg, to allow easier local template expansion syntax.
					// TODO: compat shim when missing __call
					$result = $module->invoke( '__call', $frame );

					return Validator::cleanUp( strval( $result ) );
				}
			}
			foreach ( $component->getTemplates() as $template ) {
				if ( $template->attributes->getNamedItem( 'lang' )->nodeValue === 'wikitext' ) {
					return $parser->recursiveTagParse( $template->nodeValue, $frame );
				} else {
					return '[failed to render template]';
				}
			}
		} else {
			foreach ( $component->getTemplateData() as $data ) {
				return $parser->recursiveTagParse( "<templatedata>{$data->nodeValue}</templatedata>" );
			}
		}
		return '';
	}

}
