<?php

namespace MediaWiki\Extension\ComponentTemplate;

use Scribunto_LuaLibraryBase;
use Title;

class LuaLibrary extends Scribunto_LuaLibraryBase {
	public function register() {
		$lib = [
			'expandLocalTemplate' => [ $this, 'expandLocalTemplate' ],
		];

		return $this->getEngine()->registerInterface(
			__DIR__ . '/mw.ext.ComponentTemplate.lua', $lib, []
		);
	}

	public function expandLocalTemplate( $componentTitle, $name = null, $params = null ) {
		$this->checkType( 'mw.ext.ComponentTemplate.expandLocalTemplate', 1, $componentTitle, 'string' );
		$this->checkTypeOptional( 'mw.ext.ComponentTemplate.expandLocalTemplate', 2, $name, 'string', null );
		$this->checkTypeOptional( 'mw.ext.ComponentTemplate.expandLocalTemplate', 3, $params, 'table', [] );

		// TODO: cache
		$title = Title::newFromText( $componentTitle );
		[ $componentContent, $_ ] = $this->getParser()->fetchTemplateAndTitle( $title );
		$component = Component::newFromContent( $componentContent );
		foreach ( $component->getTemplates() as $template ) {
			if ( $name === null || $template->attributes->getNamedItem( 'name' )->nodeValue === $name ) {
				// TODO: security and recursion checks
				return [ $this->getParser()->replaceVariables( $template->nodeValue, $params ) ];
			}
		}
		return [];
	}

}
