<?php

namespace MediaWiki\Extension\ComponentTemplate;

use DOMDocument;
use DOMNodeList;

class Component {

	protected $scripts;
	protected $templateData;
	protected $templates;

	public static function newFromContent( string $content ) : Component {
		// TODO: More structured parse.
		$out = new Component();

		$doc = new DOMDocument();
		$doc->loadHTML( $content, LIBXML_NOWARNING );

		$out->scripts = self::parseDomElements( $doc, 'script' );
		$out->templateData = self::parseDomElements( $doc, 'templatedata' );
		$out->templates = self::parseDomElements( $doc, 'template' );

		return $out;
	}

	private static function parseDomElements( DOMDocument $doc, string $tag ) : DOMNodeList {
		// TODO: copy to plain object model.
		return $doc->getElementsByTagName( $tag );
	}

	public function getScripts() : DOMNodeList {
		return $this->scripts;
	}

	public function getTemplateData() : DOMNodeList {
		return $this->templateData;
	}

	public function getTemplates() : DOMNodeList {
		return $this->templates;
	}

}
