<?php

namespace MediaWiki\Extension\ComponentTemplate;

use Scribunto_LuaModule;
use Scribunto_LuaStandaloneEngine;
use Title;

class ComponentAwareScribunto extends Scribunto_LuaStandaloneEngine {
	public function directlyInjectModule( string $text, Title $title ) : Scribunto_LuaModule {
		$key = $title->getPrefixedDBkey();
		$this->modules[$key] = $this->newModule( $text, $key );
		return $this->modules[$key];
	}
}
