local ComponentTemplate = {}
local php

function ComponentTemplate.expandLocalTemplate( componentTitle, templateName, args )
	return php.expandLocalTemplate( componentTitle, templateName, args )

	-- TODO: Would be nice to pass the frame here, but we pass a raw frame to PHP, nor access its ID.
	-- local frame = mw.getCurrentFrame()
	-- FIXME: "fetch", not expand
	-- local templateText = php.expandLocalTemplate( frame:getTitle(), templateName, args )
	-- childFrame = frame:newChild{ title = frame:getTitle(), args = args }
	-- return childFrame:preprocess( templateText )
end

function ComponentTemplate.setupInterface( options )
	-- Boilerplate
	ComponentTemplate.setupInterface = nil
	php = mw_interface
	mw_interface = nil

	-- Register this library in the "mw" global
	mw = mw or {}
	mw.ext = mw.ext or {}
	mw.ext.ComponentTemplate = ComponentTemplate

	package.loaded['mw.ext.ComponentTemplate'] = ComponentTemplate
end

return ComponentTemplate
